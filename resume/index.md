---
layout: resume
title: Resume
---
# Satyajit Ranjeev

##### http://satran.in | s@ranjeev.in

## Talks

Complexities of Micro Services and Event Sourcing at MicroXchg 2017 |
[https://www.infoq.com/presentations/microservices-event-sourcing](https://www.infoq.com/presentations/microservices-event-sourcing)


## Work

### Senior Software Engineer

#### OptioPay | Berlin, Germany | 2015 - Current

Building a payment gateway using micro-service architecture using Go and Kafka

- Currently building a Recommender system using Machine Learning
- Help improve conversion of our product using Statistical methods
- Cluster management using Docker, CoreOS and fleet
- Architect micro services based on event sourcing
- Worked on accounting/booking system
- Developed a HTTP Proxy through Kafka
- Contributed to Kafka driver written in Go
- Architect snapshots in event sourcing using PostgreSQL

Technologies: Go Kafka PostgreSQL CoreOS Fleet

### Software Developer 

#### Studio March | Pune, India | 2014

Built the backend for iPhone App Endless in Go. Endless recommends random articles from Wikipedia based on your likes and dislikes

- Architect and built the backend service in Go
- Implemented a recommendation engine based on Naive Bayes

Technologies: Go PostgreSQL

### Engineering Lead 

#### MQuotient | Pune, India | 2011 - 2014

Led a team of 12 to build an Intelligent Character Recognition System

- Improve prediction of words using various Statistical methods, mainly Logistic Regression and Markov processes.
- Built user interface for manual correction of incorrect recognition using JavaScript, Backbone
- Architect a distributed flow based processing using Python and RabbitMQ
- Introduced Scrum to the team

Technologies: C Python RabbitMQ MongoDB Redis JavaScript

### Head of IT 

#### Anglo Singapore International School | Bangkok, Thailand | 2008 - 2011

Led a team of 3 to migrate manual work-flow using in-house developed software and services.

- Built and maintained fault tolerant Linux based servers for file sharing, application deployment, network management and firewalls
- Architect and developed an ERP solution to manage students and staff
- Built an online book platform for the school
- Managed hosting of website

Technologies: Python MySQL Php CentOS Samba iptables RAID

### Linux Consultant 

#### SQL Star | Chennai, India | 2007 - 2008

Evangelized RedHat Enterprise Linux and trained candidates for RHCE. 

- Managed and administered RedHat Enterprise Servers
- Taught a course for RedHat Certified Engineer Exam

Technologies: RedHat Enterprise Linux Perl Xen


## Education

Master in Statistics | Madras Christian College | _2004 - 2006_
Bachelors in Statistics | Madras Christian College | _2001 - 2004_



## Projects

kafka (Go library) | [https://github.com/optiopay/kafka](https://github.com/optiopay/kafka)
sqlgen (Go postgres generator) | [https://github.com/optiopay/sqlgen](https://github.com/optiopay/sqlgen)
goimp (Go dependency) | [https://github.com/satran/goimp](https://github.com/satran/goimp)
edi (editor) | [https://github.com/satran/edi](https://github.com/satran/edi)
BK Tree (C library) |[https://bitbucket.org/ranjeev/bktree](https://bitbucket.org/ranjeev/bktree)
jsonpatch (Go library) | [https://github.com/optiopay/jsonpatch](https://github.com/optiopay/jsonpatch)


